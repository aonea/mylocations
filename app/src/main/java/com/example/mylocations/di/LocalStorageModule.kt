package com.example.mylocations.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object LocalStorageModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): Realm {
        Realm.init(context)
        val realmConfiguration = RealmConfiguration.Builder()
            .name("MyLocations")
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)

        return Realm.getDefaultInstance()
    }

}