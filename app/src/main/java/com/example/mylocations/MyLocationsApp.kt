package com.example.mylocations

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyLocationsApp: Application() {
    
}