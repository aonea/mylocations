package com.example.mylocations.data.location.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class LocationModel(
    @PrimaryKey
    var id: Long = UUID.randomUUID().mostSignificantBits,
    @SerializedName("lat")
    var lat: String? = null,
    @SerializedName("lng")
    var lng: String? = null,
    @SerializedName("label")
    var label: String? = null,
    @SerializedName("address")
    var address: String? = null,
    @SerializedName("image")
    var imgUrl: String? = null,
): RealmObject() {

}