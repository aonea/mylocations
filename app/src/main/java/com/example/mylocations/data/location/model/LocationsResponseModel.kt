package com.example.mylocations.data.location.model

import com.google.gson.annotations.SerializedName

data class LocationsResponseModel(
    @SerializedName("response")
    val status: String,
    @SerializedName("locations")
    val locations: List<LocationModel>
)