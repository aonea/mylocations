package com.example.mylocations.data.location

import com.example.mylocations.data.location.model.LocationModel
import com.example.mylocations.network.DemoApi
import io.realm.Realm
import io.realm.kotlin.where
import javax.inject.Inject

class LocationRepository @Inject constructor(
    private val api: DemoApi,
    private val db: Realm,
) {

    suspend fun getLocations(): List<LocationModel> {
        return api.getLocations().locations
            .also { locationList ->
                db.executeTransactionAsync {
                    it.insertOrUpdate(locationList)
                }
            }
    }

    suspend fun getLocationById(id: Long): LocationModel? {
        return db.where<LocationModel>().equalTo("id", id).findFirst()
    }

}