package com.example.mylocations.network

import com.example.mylocations.data.location.model.LocationsResponseModel
import retrofit2.http.GET

interface DemoApi {

    @GET("/locations")
    suspend fun getLocations(): LocationsResponseModel

}