package com.example.mylocations.view.main.recyclerview

import android.location.Location
import com.example.mylocations.data.location.model.LocationModel
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mylocations.databinding.LayoutItemLocationBinding

class LocationsAdapter(
    locations: List<LocationModel>,
    private val onItemClick: (Long) -> Unit,
    currentLocation: Location,
) :
    RecyclerView.Adapter<LocationsAdapter.ViewHolder>() {

    var locations: List<LocationModel> = locations
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var currentLocation: Location = currentLocation
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class ViewHolder(val binding: LayoutItemLocationBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutItemLocationBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = locations[position]

        with(holder.binding) {
            Glide.with(ivLocationImage.context)
                .load(item.imgUrl)
                .into(ivLocationImage)

            tvLocationAddress.text = item.address

            tvLocationDistanceToCurrent.text = computeDistance(item.lat, item.lng)

            root.setOnClickListener { onItemClick(item.id) }
        }

    }

    override fun getItemCount(): Int = locations.size

    private fun computeDistance(lat: String?, lng: String?): String {
        // TODO build Location from lat and lng

        lat?: return "Unknown distance"
        lng?: return "Unknown distance"

        val distance = FloatArray(2)

        Location.distanceBetween(
            currentLocation.latitude,
            currentLocation.longitude,
            lat.toDouble(),
            lng.toDouble(),
            distance,
        )

        return (distance[0] / 1000).toString() + "km"
    }
}