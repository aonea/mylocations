package com.example.mylocations.view.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.mylocations.data.location.LocationRepository
import com.example.mylocations.network.DemoApi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(private val locationRepository: LocationRepository): ViewModel() {

    val liveDataLocations = liveData {
        emit(locationRepository.getLocations())
    }

}