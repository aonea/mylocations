package com.example.mylocations.view.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.mylocations.data.location.LocationRepository
import com.example.mylocations.data.location.model.LocationModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LocationDetailsActivityViewModel @Inject constructor(private val locationRepository: LocationRepository) : ViewModel() {

    fun getLiveDataLocationById(locationId: Long): LiveData<LocationModel?> {
        return liveData {
            emit(locationRepository.getLocationById(locationId))
        }
    }

}