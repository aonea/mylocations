package com.example.mylocations.view.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.example.mylocations.databinding.ActivityMainBinding
import com.example.mylocations.view.location.LocationDetailsActivity
import com.example.mylocations.view.main.recyclerview.LocationsAdapter
import com.google.android.gms.location.*
import dagger.hilt.android.AndroidEntryPoint

private const val PERMISSION_REQUEST_CODE: Int = 1

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()

    private var _binding: ActivityMainBinding? = null
    private val binding
        get() = _binding!!

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var liveDataCurrentLocation: MutableLiveData<Location?> = MutableLiveData(null)
    private var liveDataLastKnownLocation: MutableLiveData<Location?> = MutableLiveData(null)

    private val onLocationItemClicked: (Long) -> Unit = { locationId ->
        goToLocationDetailActivity(locationId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        checkLocationPermission()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        updateLastKnowLocation()

        liveDataLastKnownLocation.observe(this) { lastLocation ->
            lastLocation?.let {
                initializeUi(lastLocation)
                    .also { requestLocationUpdates() }
            }
        }
        liveDataCurrentLocation.observe(this) { location ->
            location?.let { updateUi(it) }
        }

    }

    private fun updateUi(currentLocation: Location) {
        binding.apply {
            rvLocations.adapter?.let {
                (it as LocationsAdapter).currentLocation = currentLocation
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun updateLastKnowLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener {
                it?.let {
                    liveDataLastKnownLocation.value = it
                }
            }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates() {
        val locationRequest = LocationRequest.create().apply {
            interval = 5000
            fastestInterval = 2500
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    liveDataCurrentLocation.value = locationResult.lastLocation
                }
            },
            mainLooper
        )
    }

    private fun initializeUi(location: Location) {
        setupRecyclerView()

        viewModel.liveDataLocations
            .observe(this@MainActivity) { locations ->
                binding.apply {
                    rvLocations.adapter =
                        LocationsAdapter(locations, onLocationItemClicked, location)
                }
            }

    }

    private fun setupRecyclerView() {

        with(binding) {
            rvLocations.layoutManager =
                LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
            val snapHelper = LinearSnapHelper()
            snapHelper.attachToRecyclerView(rvLocations)
        }

    }

    private fun goToLocationDetailActivity(locationId: Long) {
        val intent = Intent(this, LocationDetailsActivity::class.java).also {
            it.putExtra("id", locationId)
        }

        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    private fun checkLocationPermission() {
        if (
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder(this)
                    .setTitle("Location Permission")
                    .setMessage("Please allow for location usage in order for this application to work")
                    .setPositiveButton("Allow") { _, _ ->
                        requestLocationPermission()
                    }
                    .setNegativeButton("Cancel") { _, _ ->
                        finish()
                    }
                    .create()
                    .show()
            } else {
                requestLocationPermission()
            }
        }
    }

    private fun requestLocationPermission() = ActivityCompat.requestPermissions(
        this,
        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
        PERMISSION_REQUEST_CODE
    )
}