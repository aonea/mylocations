package com.example.mylocations.view.location

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.mylocations.data.location.model.LocationModel
import com.example.mylocations.databinding.ActivityLocationDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LocationDetailsActivity: AppCompatActivity() {

    private val viewModel: LocationDetailsActivityViewModel by viewModels()

    private var _binding: ActivityLocationDetailBinding? = null
    private val binding
        get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLocationDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        println("detail activity: ${intent?.extras?.getLong("id")}")
        intent?.extras?.getLong("id")?.let { locationId ->
            viewModel.getLiveDataLocationById(locationId)
                .observe(this) { location ->
                    location?.let { populateUi(it) }
                }
        }

    }

    private fun populateUi(location: LocationModel) {

        binding.apply {
            Glide.with(this@LocationDetailsActivity)
                .load(location.imgUrl)
                .into(ivLocationDetailsImage)

            tvLocationDetailsAddress.text = location.address
            tvLocationDetailsLabel.text = location.label
            tvLocationDetailsLat.text = "lat: ${location.lat}"
            tvLocationDetailsLng.text = "lng: ${location.lng}"
        }

    }

}